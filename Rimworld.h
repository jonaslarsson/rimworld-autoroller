#ifndef RIMWORLD_H
#define RIMWORLD_H

#include <QObject>

namespace Rimworld
{
    enum Skills
    {
        Shooting,
        Melee,
        Construction,
        Mining,
        Cooking,
        Plants,
        Animals,
        Crafting,
        Artistic,
        Medical,
        Social,
        Intellectual
    };
    enum Passion
    {
        PassionNone,
        PassionInterested,
        PassionBurning
    };

    const int SkillCount = 12;
    QString skillName(int skillIndex);
    int skillIndex(QString name);
    QStringList traits();
    QStringList incapabilities();

    struct CharacterInfo
    {
        int skills[SkillCount];
        Passion passions[SkillCount];
        QStringList incapabilities;
        QStringList traits;
    };
    bool operator==(CharacterInfo a, CharacterInfo b);

    void randomizeCharacter();
    bool isGameActive();
    bool characterInfo(CharacterInfo *characterInfo);
    bool characterInfo(QImage screenshot, CharacterInfo *characterInfo);
    void rollForHighSkill();
    void rollForDecodingTest();
};

#endif // RIMWORLD_H
