import QtQuick 2.0

Rectangle {
    width: 100
    height: 100
    color: "#878787"

    Rectangle {
        anchors.fill: parent
        anchors.margins: 1
        color: "#2a2b2c"
    }
}
