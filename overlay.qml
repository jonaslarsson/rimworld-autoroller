import QtQuick 2.0

Rectangle {
    color: "#616c7a"
    visible: true

    Rectangle {
        anchors.fill: parent
        anchors.margins: 1
        color: "#15191d"
    }

    Text {
        x: 18
        y: 20
        text: "Character roller"
        color: "#FFFFFF"
        font.family: "Arial"
        font.pixelSize: 22
    }

    Group {
        anchors.fill: parent
        anchors.leftMargin: 22
        anchors.rightMargin: 22
        anchors.topMargin: 65
        anchors.bottomMargin: 75

        Column {
            anchors.fill: parent
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            spacing: 10

            Item {
                width: 10
                height: 5
            }

            Text {
                text: "Minimum skill levels"
                color: "#FFFFFF"
                font.family: "Arial"
                font.pixelSize: 22
            }

            Column {
                Repeater {
                    model: MainWindow.skills
                    Skill {
                        name: modelData
                        skillLevel: 0
                        onPassionLevelChanged: MainWindow.setPassionLevel(modelData, passionLevel)
                        onSkillLevelChanged: MainWindow.setSkillLevel(modelData, skillLevel)
                        Component.onCompleted: {
                            MainWindow.setPassionLevel(modelData, passionLevel)
                            MainWindow.setSkillLevel(modelData, skillLevel)
                        }
                    }
                }
            }

            Text {
                text: "Required capable of"
                color: "#FFFFFF"
                font.family: "Arial"
                font.pixelSize: 22
            }

            Grid {
                spacing: 5
                rows: 5
                Repeater {
                    model: MainWindow.incapabilities
                    Label {
                        text: modelData
                        onUpdated: {
                            MainWindow.setRequiredCapableOf(modelData, selected)
                        }
                    }
                }
            }

            Text {
                text: "Required traits"
                color: "#FFFFFF"
                font.family: "Arial"
                font.pixelSize: 22
            }

            Grid {
                width: parent.width
                spacing: 5
                rows: 5
                Repeater {
                    model: ["Great memory", "Jogger", "Tough", "Industrious",
                            "Nimble", "Brawler", "Sanguine", "Fast learner",
                            "Transhumanist", "Psychopath", "Cannibal", "Quick sleeper"]
                    Label {
                        text: modelData
                        selected: false
                        onUpdated: {
                            MainWindow.setRequiredTrait(modelData, selected)
                        }
                    }
                }
            }

            Text {
                text: "Disallowed traits"
                color: "#FFFFFF"
                font.family: "Arial"
                font.pixelSize: 22
            }

            Grid {
                spacing: 5
                rows: 4
                Repeater {
                    model: ["Pyromaniac", "Slowpoke", "Brawler", "Slow learner", "Slothful", "Sickly", "Wimp", "Jealous", "Lazy", "Depressive", "Body purist"]
                    Label {
                        text: modelData
                        selected: modelData === "Brawler" ? false : true
                        onUpdated: {
                            MainWindow.setDisallowedTrait(modelData, selected)
                        }
                    }
                }
            }
        }
    }

    Text {
        x: 18
        y: 50
        text: MainWindow.debugText
        color: "#FFFFFF"
        font.family: "Arial"
        font.pixelSize: 14
    }

    Button {
        x: 125
        y: 807
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        text: "Roll"
        onClicked: MainWindow.onRoll()
    }
}
