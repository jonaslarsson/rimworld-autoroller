import QtQuick 2.0

Item {
    width: 230
    height: 24
    property alias name: skillName.text
    property alias passionLevel: passion.level
    property alias skillLevel: skill.level

    MouseArea {
        id: mouseArea
        hoverEnabled: true
        anchors.fill: parent
    }

    Rectangle {
        anchors.fill: parent
        color: "#404142"
        visible: mouseArea.containsMouse
    }

    Passion {
        id: passion
        x: 92
        selected: mouseArea.containsMouse
    }

    SkillLevel {
        id: skill
        x: 116
        level: -1
    }

    Text {
        id: skillName
        y: 3
        x: 6
        color: "#FFFFFF"
        font.family: "Arial"
        font.pixelSize: 14
    }
}
