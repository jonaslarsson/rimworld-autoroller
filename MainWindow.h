#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "OverlayWindow.h"
#include "Rimworld.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_PROPERTY(QString debugText READ debugText WRITE setDebugText NOTIFY debugTextChanged)
    Q_PROPERTY(QStringList traits READ traits CONSTANT)
    Q_PROPERTY(QStringList incapabilities READ incapabilities CONSTANT)
    Q_PROPERTY(QStringList skills READ skills CONSTANT)

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString debugText() const;
    QStringList traits() const;
    QStringList incapabilities() const;
    QStringList skills() const;

public slots:
    void onRoll();
    void setPassionLevel(QString skill, int passionLevel);
    void setSkillLevel(QString skill, int skillLevel);
    void setRequiredCapableOf(QString capability, bool selected);
    void setRequiredTrait(QString trait, bool selected);
    void setDisallowedTrait(QString trait, bool selected);
    void setDebugText(QString debugText);

signals:
    void debugTextChanged(QString debugText);

private slots:
    void onTimer();

private:
    bool foundCharacter(const Rimworld::CharacterInfo& character);
    Ui::MainWindow *m_gui;
    QTimer m_timer;
    OverlayWindow *m_overlayWindow;
    QString m_debugText;
    QStringList m_requiredTraits;
    QStringList m_disallowedTraits;
    QStringList m_requiredCapabilities;
    int m_requiredSkillLevels[Rimworld::SkillCount];
    int m_requiredPassionLevels[Rimworld::SkillCount];
};
#endif // MAINWINDOW_H
