import QtQuick 2.0

Item {
    width: 24
    height: 24
    property int level: 0
    property bool selected: false

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (level === 2)
            {
                level = 0;
            } else {
                level++;
            }
        }
    }

    Image {
        source: "images/passion_none.png"
        visible: level === 0 && !selected
    }

    Image {
        source: "images/passion_none_selected.png"
        visible: level === 0 && selected
    }

    Image {
        source: "images/passion_interest.png"
        visible: level === 1 && !selected
    }

    Image {
        source: "images/passion_interest_selected.png"
        visible: level === 1 && selected
    }

    Image {
        source: "images/passion_burning.png"
        visible: level === 2 && !selected
    }

    Image {
        source: "images/passion_burning_selected.png"
        visible: level === 2 && selected
    }
}
