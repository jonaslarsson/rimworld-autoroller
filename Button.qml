import QtQuick 2.0

Item
{
    id: root
    width: 150
    height: 38
    property alias text: label.text
    signal clicked

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: root.clicked()
    }

    BorderImage {
        id: button
        anchors.fill: parent
        source: "images/rimbutton.png"
        border.left: 6; border.top: 6
        border.right: 6; border.bottom: 6
    }

    Text {
        id: label
        text: "Button"
        color: "#FFFFFF"
        font.family: "Arial"
        font.pixelSize: 14
        anchors.centerIn: parent
        anchors.bottomMargin: 1
    }

    states: [
        State {
            name: "pressed"
            when: mouseArea.pressed && mouseArea.containsMouse

            PropertyChanges {
                target: button
                source: "images/rimbutton_pressed.png"
            }
        },
        State {
            name: "hover"
            when: mouseArea.containsMouse

            PropertyChanges {
                target: button
                source: "images/rimbutton_hover.png"
            }
        }
    ]
}
