import QtQuick 2.0

Item {
    id: root
    width: 114
    height: 24
    property int level: -1

    MouseArea {
        anchors.fill: parent
        onPressed: {
            setLevelFromPosition(mouse.x)
        }
        onPositionChanged: {
            setLevelFromPosition(mouse.x)
        }
    }

    function setLevelFromPosition(x)
    {
        const widths = [1, 6, 11, 17, 23, 28, 34, 40, 46, 51, 57, 63, 68, 74, 80, 85, 91, 97, 103, 108, 114]
        if (x < -4)
        {
            root.level = -1;
            return;
        }

        for (var i = 0; i <= 20; i++)
        {
            if (x < widths[i] + 2)
            {
                root.level = i;
                return;
            }
        }
    }

    onLevelChanged: {
        const widths = [1, 6, 11, 17, 23, 28, 34, 40, 46, 51, 57, 63, 68, 74, 80, 85, 91, 97, 103, 108, 114]
        if (level < 0)
        {
            levelBar.width = 0
        }
        else
        {
            levelBar.width = widths[level]
        }
    }

    Rectangle {
        id: levelBar
        height: parent.height
        width: 0
        color: "#535455"
    }

    Text {
        x: 4
        y: 5
        text: root.level >= 0 ? root.level : "-"
        color: root.level >= 0 ? "#FFFFFF" : "#888888"
        font.family: "Arial"
        font.pixelSize: 14
    }

}
