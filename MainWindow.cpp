#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Rimworld.h"
#include "SystemAutomation.h"

#include <QDebug>
#include <QThread>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_gui(new Ui::MainWindow)
{
    m_overlayWindow = nullptr;
    m_gui->setupUi(this);
    connect(&m_timer, &QTimer::timeout, this, &MainWindow::onTimer);
    m_timer.setSingleShot(false);
    m_timer.start(100);


    for (int i = 0; i < Rimworld::SkillCount; i++)
    {
        m_requiredSkillLevels[i] = 0;
        m_requiredPassionLevels[i] = 0;
    }

    m_overlayWindow = new OverlayWindow(this);
}

MainWindow::~MainWindow()
{
    delete m_overlayWindow;
    delete m_gui;
}

QString MainWindow::debugText() const
{
    return m_debugText;
}

QStringList MainWindow::traits() const
{
    return Rimworld::traits();
}

QStringList MainWindow::incapabilities() const
{
    return Rimworld::incapabilities();
}

QStringList MainWindow::skills() const
{
    QStringList skills;
    for (int i = 0; i < Rimworld::SkillCount; i++)
    {
        skills += Rimworld::skillName(i);
    }
    return skills;
}

void MainWindow::onRoll()
{
    int delay = 5;
    int count = 1000;
    Rimworld::CharacterInfo oldCharacterInfo;
    while (count--)
    {
        QImage screenshot = SystemAutomation::grabScreen();
        Rimworld::CharacterInfo characterInfo;
        if (Rimworld::characterInfo(screenshot, &characterInfo) == false) return;
        while (characterInfo == oldCharacterInfo)
        {
            delay += 5;
            qDebug() << "RUNNING TOO FAST!!! Adjusting delay to" << delay;
            screenshot = SystemAutomation::grabScreen();
            if (Rimworld::characterInfo(screenshot, &characterInfo) == false) return;
        }
        oldCharacterInfo = characterInfo;


        if (foundCharacter(characterInfo))
        {
            // Done!
            return;
        }

        Rimworld::randomizeCharacter();
        QThread::msleep(delay);
    }
}

void MainWindow::setPassionLevel(QString skill, int passionLevel)
{
    int skillIndex = Rimworld::skillIndex(skill);
    if (skillIndex >= 0)
    {
        m_requiredPassionLevels[skillIndex] = passionLevel;
    }
    else
    {
        qCritical() << "Cant find index of skill" << skill;
    }
}

void MainWindow::setSkillLevel(QString skill, int skillLevel)
{
    int skillIndex = Rimworld::skillIndex(skill);
    if (skillIndex >= 0)
    {
        m_requiredSkillLevels[skillIndex] = skillLevel;
    }
    else
    {
        qCritical() << "Cant find index of skill" << skill;
    }
}

void MainWindow::setRequiredCapableOf(QString capability, bool selected)
{
    if (selected)
    {
        m_requiredCapabilities.append(capability);
    }
    else
    {
        m_requiredCapabilities.removeAll(capability);
    }
}

void MainWindow::setRequiredTrait(QString trait, bool selected)
{
    if (selected)
    {
        m_requiredTraits.append(trait);
    }
    else
    {
        m_requiredTraits.removeAll(trait);
    }
}

void MainWindow::setDisallowedTrait(QString trait, bool selected)
{
    if (selected)
    {
        m_disallowedTraits.append(trait);
    }
    else
    {
        m_disallowedTraits.removeAll(trait);
    }
}

void MainWindow::setDebugText(QString debugText)
{
    if (m_debugText == debugText)
        return;

    m_debugText = debugText;
    emit debugTextChanged(m_debugText);
}

void MainWindow::onTimer()
{
    if (Rimworld::isGameActive())
    {
        if (m_overlayWindow->isHidden())
        {
            m_overlayWindow->show();
        }
    }
    else
    {
        if (m_overlayWindow->isHidden() == false)
        {
            m_overlayWindow->hide();
        }
    }
}

bool MainWindow::foundCharacter(const Rimworld::CharacterInfo &character)
{
    for (int i = 0; i < Rimworld::SkillCount; i++)
    {
        if (character.skills[i] < m_requiredSkillLevels[i]) return false;
        if (character.passions[i] < m_requiredPassionLevels[i]) return false;
    }

    foreach (QString incapability, character.incapabilities)
    {
        if (m_requiredCapabilities.contains(incapability)) return false;
    }

    foreach (QString trait, character.traits)
    {
        if (m_disallowedTraits.contains(trait)) return false;
    }

    foreach (QString requiredTrait, m_requiredTraits)
    {
        if (character.traits.contains(requiredTrait) == false) return false;
    }

    return true;
}
