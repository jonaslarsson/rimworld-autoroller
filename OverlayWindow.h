#ifndef OVERLAYWINDOW_H
#define OVERLAYWINDOW_H

#include <QMainWindow>

namespace Ui {
class OverlayWindow;
}

class OverlayWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit OverlayWindow(QWidget *parent = nullptr);
    ~OverlayWindow();

private:
//    Ui::OverlayWindow *m_gui;
};

#endif // OVERLAYWINDOW_H
