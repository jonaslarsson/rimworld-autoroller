#include "SystemAutomation.h"
#include <QScreen>
#include <QDebug>
#include <QPixmap>
#include <QGuiApplication>
#include <windows.h>

SystemAutomation::SystemAutomation()
{
}

bool SystemAutomation::mouseMove(int x, int y)
{
    QSize screenSize = QGuiApplication::primaryScreen()->availableSize();
    INPUT input;
    input.type = INPUT_MOUSE;
    input.mi.dx = static_cast<LONG>(static_cast<double>(x) * 65535.0 / static_cast<double>(screenSize.width()));
    input.mi.dy = static_cast<LONG>(static_cast<double>(y) * 65535.0 / static_cast<double>(screenSize.height()));
    input.mi.mouseData = 0;
    input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;
    input.mi.time = 0;
    input.mi.dwExtraInfo = 0;
    return SendInput(1, &input, sizeof(INPUT)) == 1;
}

bool SystemAutomation::mouseClick(int x, int y, Qt::MouseButton mouseButton, int clicks)
{
    if (!mouseMove(x, y)) return false;
    if (clicks < 0) return false;

    DWORD buttonDown, buttonUp;
    switch (mouseButton)
    {
    case Qt::LeftButton:
        buttonDown = MOUSEEVENTF_LEFTDOWN;
        buttonUp = MOUSEEVENTF_LEFTUP;
        break;
    case Qt::RightButton:
        buttonDown = MOUSEEVENTF_RIGHTDOWN;
        buttonUp = MOUSEEVENTF_RIGHTUP;
        break;
    case Qt::MiddleButton:
        buttonDown = MOUSEEVENTF_MIDDLEDOWN;
        buttonUp = MOUSEEVENTF_MIDDLEUP;
        break;
    default:
        return false;
    }

    while (clicks--)
    {
        INPUT input[2];
        input[0].type = INPUT_MOUSE;
        input[0].mi.dx = 0;
        input[0].mi.dy = 0;
        input[0].mi.mouseData = 0;
        input[0].mi.dwFlags = buttonDown;
        input[0].mi.time = 0;
        input[0].mi.dwExtraInfo = 0;
        input[1].type = INPUT_MOUSE;
        input[1].mi.dx = 0;
        input[1].mi.dy = 0;
        input[1].mi.mouseData = 0;
        input[1].mi.dwFlags = buttonUp;
        input[1].mi.time = 0;
        input[1].mi.dwExtraInfo = 0;
        if (SendInput(2, input, sizeof(INPUT)) != 2) return false;
    }
    return true;
}

QString SystemAutomation::activeWindowName()
{
    wchar_t wszBuffer[256];
    HWND hwnd = GetForegroundWindow();
    int ret = GetWindowText(hwnd, wszBuffer, sizeof(wszBuffer) / sizeof(wchar_t));
    if (ret == 0) return QString();
    return QString::fromWCharArray(wszBuffer);
}

QImage SystemAutomation::grabScreen()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    return screen->grabWindow(0).toImage();
}
