#include "Rimworld.h"
#include "SystemAutomation.h"
#include <QTextStream>
#include <QDebug>
#include <QThread>
#include <QStandardPaths>
#include <QList>
#include <QDir>

const int LabelHeight = 22;
const QRgb ColorBg = 0xff2a2b2c;
const QRgb ColorLabel = 0xff404142;

static QMap<QString, QImage> incapabilitiesImageCache;
static QMap<QString, QImage> traitsImageCache;

static void fillImageCacheIfNeeded()
{
    if (incapabilitiesImageCache.isEmpty())
    {
        foreach (const QString &imageName, QDir(":/incapable").entryList())
        {
            QString name = imageName.left(imageName.size() - 4);
            QImage image = QImage(":/incapable/" + imageName);
            if (image.isNull())
            {
                qCritical() << "Failed to load image" << imageName;
            }
            incapabilitiesImageCache.insert(name, image);
        }
    }

    if (traitsImageCache.isEmpty())
    {
        foreach (const QString &imageName, QDir(":/traits").entryList())
        {
            QString name = imageName.left(imageName.size() - 4);
            QImage image = QImage(":/traits/" + imageName);
            if (image.isNull())
            {
                qCritical() << "Failed to load image" << imageName;
            }
            else
            {
                for (int y = 0; y < image.height(); y++)
                {
                    if (image.pixel(0, y) != ColorLabel)
                    {
                        qCritical() << "Corrupt image found" << imageName;
                    }
                }
                traitsImageCache.insert(name, image);
            }
        }
    }
}


static void dumpImage(QString name, QImage image)
{
    QString folder = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    image.save(folder + "/" + name + ".png");
}

QString translateIncapableImage(QImage image)
{
    fillImageCacheIfNeeded();
    foreach (QString name, incapabilitiesImageCache.keys())
    {
        if (image == incapabilitiesImageCache.value(name)) return name;
    }

    return QString();
}

QString translateTraitImage(QImage image)
{
    fillImageCacheIfNeeded();

    foreach (QString name, traitsImageCache.keys())
    {
        if (image == traitsImageCache.value(name)) return name;
    }

    return QString();
}

static QImage extractLabel(QImage image, int x, int y)
{
    if (image.pixel(x, y) == ColorBg) return QImage();

    int width = 0;
    while (image.pixel(x + width, y) == ColorLabel)
    {
        width++;
        if (width >= image.width())
        {
            qDebug() << "Outside image bounds while extracting label";
            return QImage();
        }
    }

    if (image.pixel(x + width, y) == ColorBg)
    {
        return image.copy(x, y, width, LabelHeight);
    }

    qDebug() << "Failed to extract label, width:" << width;
    return QImage();
}

static QList<QImage> imageSplitLine(QImage image, int y)
{
    const int LabelSpacing = 5;

    QList<QImage> images;

    int x = 0;
    QImage label = extractLabel(image, x, y);

    while (label.isNull() == false)
    {
        images.append(label);
        x += label.width() + LabelSpacing;
        label = extractLabel(image, x, y);
    }

    return images;
}

static QList<QImage> imageSplit(QImage image)
{
    QList<QImage> images;
    for (int y = 0; y < image.height(); y++)
    {
        if (image.pixel(0, y) == ColorLabel)
        {
            images += imageSplitLine(image, y);
            y += LabelHeight;
        }
    }
    return images;
}

static int skillbarWidthToLevel(int width)
{
    const int Widths[21] = { 1, 6, 11, 17, 23, 28, 34, 40, 46, 51, 57,
                             63, 68, 74, 80, 85, 91, 97, 103, 108, 114};
    if (width == 0) return -1;
    for (int i = 0; i < 21; i++)
    {
        if (width == Widths[i]) return i;
    }
    return -2;
}

static int skillbarWidth(QImage image, int skillIndex)
{
    const int StartX = 995;
    const int SkillY = 340 + skillIndex * 27;
    const QRgb SkillColorBg = 0xff2a2b2c;
    const QRgb SkillColorFg = 0xff404142;

    for (int i = 0; i < 120; i++)
    {
        QRgb color = image.pixel(StartX + i, SkillY);
        if (color == SkillColorBg)
            return i;
        if (color != SkillColorFg)
            return -1;
    }
    return -2;
}

static bool skillPassion(QImage image, int skillIndex, Rimworld::Passion *passion)
{
    const int SkillY = 347 + skillIndex * 27;
    QRgb p1 = image.pixel(977, SkillY);
    QRgb p2 = image.pixel(982, SkillY);

    if (p1 == 0xfffff363 && p2 == 0xfff79939)
    {
        *passion = Rimworld::PassionBurning;
    }
    else if (p1 == 0xff2a2b2c && p2 == 0xfffff363)
    {
        *passion = Rimworld::PassionInterested;
    }
    else if (p1 == 0xff2a2b2c && p2 == 0xff2a2b2c)
    {
        *passion = Rimworld::PassionNone;
    }
    else
    {
        return false;
    }

    return true;
}

void Rimworld::randomizeCharacter()
{
    SystemAutomation::mouseClick(1355, 250);
}

bool Rimworld::isGameActive()
{
    if (SystemAutomation::activeWindowName() == "RimWorld by Ludeon Studios")
        return true;
    return false;
}

bool Rimworld::characterInfo(Rimworld::CharacterInfo *characterInfo)
{
    QImage screenshot = SystemAutomation::grabScreen();
    return Rimworld::characterInfo(screenshot, characterInfo);
}

bool Rimworld::characterInfo(QImage screenshot, Rimworld::CharacterInfo *characterInfo)
{
    for (int i = 0; i < SkillCount; i++)
    {
        int skillWidth = skillbarWidth(screenshot, i);
        if (skillWidth < 0)
        {
            dumpImage("rimworld", screenshot);
            qCritical() << "Failed to get width for skill" << i;
            return false;
        }
        int skillLevel = skillbarWidthToLevel(skillWidth);
        if (skillLevel < -1)
        {
            qCritical() << "Invalid skill width " << skillWidth;
            return false;
        }
        characterInfo->skills[i] = skillLevel;

        if (skillPassion(screenshot, i, &characterInfo->passions[i]) == false)
        {
            dumpImage("rimworld", screenshot);
            qCritical() << "Failed to get passion on skill" << i;
            return false;
        }
    }

    QImage incapableImage = screenshot.copy(629, 484, 250, 95);
    QImage traitsImage = screenshot.copy(629, 603, 250, 95);

    characterInfo->incapabilities.clear();
    characterInfo->traits.clear();

    int count = 0;
    foreach (QImage image, imageSplit(incapableImage))
    {
        count++;
        QString incapability = translateIncapableImage(image);
        if (incapability.isNull())
        {
            qCritical() << "No match for incapability, dumping image";
            dumpImage("incapable_missing", image);
            return false;
        }
        characterInfo->incapabilities += incapability;
    }

    count = 0;
    foreach (QImage image, imageSplit(traitsImage))
    {
        count++;
        QString trait = translateTraitImage(image);
        if (trait.isNull())
        {
            qCritical() << "No match for trait, dumping image";
            dumpImage("trait_missing", image);
            return false;
        }
        characterInfo->traits += trait;
    }

    return true;
}

QString Rimworld::skillName(int skillIndex)
{
    const char *SkillName[SkillCount] = { "Shooting", "Melee", "Construction", "Mining",
                                          "Cooking", "Plants", "Animals", "Crafting",
                                          "Artistic", "Medical", "Social", "Intellectual" };
    if (skillIndex >= 0 && skillIndex <= SkillCount)
        return QString(SkillName[skillIndex]);
    return QString();
}

int Rimworld::skillIndex(QString name)
{
    for (int i = 0; i < SkillCount; i++)
    {
        if (name == skillName(i)) return i;
    }
    return -1;
}

QStringList Rimworld::traits()
{
    fillImageCacheIfNeeded();
    return traitsImageCache.keys();
}

QStringList Rimworld::incapabilities()
{
    fillImageCacheIfNeeded();
    return incapabilitiesImageCache.keys();
}

void Rimworld::rollForHighSkill()
{
    int delay = 5;
    int count = 1000;
    Rimworld::CharacterInfo oldCharacterInfo;
    while (count--)
    {
        QImage screenshot = SystemAutomation::grabScreen();
        Rimworld::CharacterInfo characterInfo;
        if (Rimworld::characterInfo(screenshot, &characterInfo) == false) return;
        while (characterInfo == oldCharacterInfo)
        {
            delay += 5;
            qDebug() << "RUNNING TOO FAST!!! Adjusting delay to" << delay;
            screenshot = SystemAutomation::grabScreen();
            if (Rimworld::characterInfo(screenshot, &characterInfo) == false) return;
        }
        oldCharacterInfo = characterInfo;

        for (int i = 0; i < SkillCount; i++)
        {
            if (characterInfo.skills[i] > 91)
            {
                qDebug() << "Found skill" << characterInfo.skills[i];
                dumpImage("rimworld", screenshot);
                return;
            }
        }
        randomizeCharacter();
        QThread::msleep(delay);
    }
}

bool Rimworld::operator==(Rimworld::CharacterInfo a, Rimworld::CharacterInfo b)
{
    for (int i = 0; i < SkillCount; i++)
    {
        if (a.skills[i] != b.skills[i]) return false;
    }
    return true;
}

void Rimworld::rollForDecodingTest()
{
    int delay = 5;
    int count = 200;
    Rimworld::CharacterInfo oldCharacterInfo;
    while (count--)
    {
        QImage screenshot = SystemAutomation::grabScreen();
        Rimworld::CharacterInfo characterInfo;
        if (Rimworld::characterInfo(screenshot, &characterInfo) == false)
        {
            qDebug() << "Found character that cant be decoded";
            dumpImage("rimworld", screenshot);
            return;
        }

        while (characterInfo == oldCharacterInfo)
        {
            delay += 5;
            qDebug() << "RUNNING TOO FAST!!! Adjusting delay to" << delay;
            screenshot = SystemAutomation::grabScreen();
            if (Rimworld::characterInfo(screenshot, &characterInfo) == false) return;
        }
        oldCharacterInfo = characterInfo;

        randomizeCharacter();
        QThread::msleep(delay);
    }
}
