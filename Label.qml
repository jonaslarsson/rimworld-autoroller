import QtQuick 2.0

Item {
    height: 22
    width: label.width + 10
    property alias text: label.text
    property bool selected: true
    signal updated

    Component.onCompleted: {
        updated()
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            selected = !selected
            updated()
        }
    }

    Rectangle {
        color: mouseArea.containsMouse ? "#535455" : "#404142"
        anchors.fill: parent
    }

    Text {
        id: label
        y: 3
        x: 5
        color: selected ? "#FFFFFF" : "#888888"
        font.family: "Arial"
        font.pixelSize: 14
        font.strikeout: !selected
    }
}
