#include "OverlayWindow.h"
#include "ui_OverlayWindow.h"
#include <windows.h>

#include <QQuickWidget>
#include <QQmlContext>

OverlayWindow::OverlayWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setWindowFlags( windowFlags() | Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);
//    setWindowFlags( windowFlags() | Qt::Window | Qt::FramelessWindowHint | Qt::WindowTransparentForInput | Qt::WindowStaysOnTopHint );
    this->move(1495, 58);
    this->resize(400, 964);

    QQuickWidget *view = new QQuickWidget(this);
    const QUrl url(QStringLiteral("qrc:/overlay.qml"));
    view->rootContext()->setContextProperty("MainWindow", parent);
    view->setSource(url);
    view->setResizeMode(QQuickWidget::SizeRootObjectToView);
    view->setGeometry(QRect(0, 0, this->width(), this->height()));
    view->show();


    SetWindowLongPtrW(HWND(this->winId()), GWL_EXSTYLE, WS_EX_NOACTIVATE);
}

OverlayWindow::~OverlayWindow()
{
//    delete m_gui;
}
