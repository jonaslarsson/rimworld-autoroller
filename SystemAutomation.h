#ifndef SYSTEMAUTOMATION_H
#define SYSTEMAUTOMATION_H

#include <QObject>
#include <QImage>

class SystemAutomation
{
    explicit SystemAutomation();
public:
    static bool mouseMove(int x, int y);
    static bool mouseClick(int x, int y, Qt::MouseButton mouseButton = Qt::LeftButton, int clicks = 1);
    static QString activeWindowName();
    static QImage grabScreen();
};

#endif // SYSTEMAUTOMATION_H
